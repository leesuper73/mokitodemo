package com.facebook.exercises;

import java.util.HashMap;

public class ReverseMakeEqual {
	public static void main(String[] args) {

		int[] array_a_1 = { 1, 2, 3, 4 };
		int[] array_b_1 = { 1, 4, 3, 2 };

		int[] array_a_2 = { 1, 2, 3, 4 };
		int[] array_b_2 = { 1, 4, 3, 3 };

		areTheyEqual(array_a_2, array_b_2);
	}

	private static boolean areTheyEqual(int[] array_a, int[] array_b) {
		boolean result = true;

		HashMap<Integer, Integer> aMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> bMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < array_a.length; i++) {
			if (aMap.get(array_a[i]) == null) {
				aMap.put(array_a[i], 1);
			} else {
				int count = aMap.get(array_a[i]);
				aMap.put(array_a[i], ++count);
			}
		}

		System.out.println("aMap " + aMap.toString());

		for (int i = 0; i < array_b.length; i++) {
			if (bMap.get(array_b[i]) == null) {
				bMap.put(array_b[i], 1);
			} else {
				int count = bMap.get(array_b[i]);
				bMap.put(array_b[i], ++count);
			}
		}

		System.out.println("bMap " + bMap.toString());

		for (Integer key : aMap.keySet()) {
			if (aMap.get(key) != bMap.get(key)) {
				result = false;
				break;
			}
		}

		System.out.println("Result " + result);
		return result;
	}
}