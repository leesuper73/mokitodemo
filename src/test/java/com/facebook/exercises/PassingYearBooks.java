package com.facebook.exercises;

import java.util.HashMap;

public class PassingYearBooks {
	public static void main(String[] args) {

		int[] arr_1 = { 2, 1 };
		int[] arr_2 = { 1, 2 };

		findSignatureCounts(arr_1);
	}

	private static int[] findSignatureCounts(int[] arr) {
		int[] result = new int[arr.length];

		HashMap<Integer, Integer> aMap = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> bMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < arr.length; i++) {
			if (aMap.get(arr[i]) == null) {
				aMap.put(arr[i], 1);
			} else {
				int count = aMap.get(arr[i]);
				aMap.put(arr[i], ++count);
			}
		}

		System.out.println("aMap " + aMap.toString());

		System.out.println("Result " + result);
		return result;
	}
}