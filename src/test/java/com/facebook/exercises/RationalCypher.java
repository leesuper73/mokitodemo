package com.facebook.exercises;

import java.util.Arrays;
import java.util.List;

public class RationalCypher {
	public static void main(String[] args) {

		rotationalCipher("abcdZXYzxy-999.@", 200);
	}

	private static String rotationalCipher(String input, int rotationFactor) {
		StringBuffer cyphered = new StringBuffer();

		String[] upper = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
				"S", "T", "U", "V", "W", "X", "Y", "Z" };
		List<String> upperList = Arrays.asList(upper);
		String[] lower = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
				"s", "t", "u", "v", "w", "x", "y", "z" };
		List<String> lowerList = Arrays.asList(lower);
		String[] numbers = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
		List<String> numbersList = Arrays.asList(numbers);

		try {
			int numsLimit = 10;
			int alphaLimit = 26;
			char[] inputChar = input.toCharArray();

			for (char character : inputChar) {
				int index = 0;
				if (upperList.indexOf(String.valueOf(character)) > -1) { // Upper
					index = (upperList.indexOf(String.valueOf(character)) + rotationFactor) % alphaLimit;
					cyphered.append(upper[index]);
				} else if (lowerList.indexOf(String.valueOf(character)) > -1) { // Lower
					index = (lowerList.indexOf(String.valueOf(character)) + rotationFactor) % alphaLimit;
					cyphered.append(lower[index]);
				} else if (numbersList.indexOf(String.valueOf(character)) > -1) { // Number
					index = (numbersList.indexOf(String.valueOf(character)) + rotationFactor) % numsLimit;
					cyphered.append(numbers[index]);
				} else {
					cyphered.append(String.valueOf(character));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(cyphered.toString());
		return cyphered.toString();
	}
}