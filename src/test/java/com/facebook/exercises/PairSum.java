package com.facebook.exercises;

import java.util.HashMap;
import java.util.Map;

public class PairSum {
	public static void main(String[] args) {

		int k_1 = 6;
		int[] arr_1 = { 1, 2, 3, 4, 3 };

		int k_2 = 6;
		int[] arr_2 = { 1, 5, 3, 3, 3 };
		numberOfWays(arr_2, k_2);
	}

	private static int numberOfWays(int[] arr, int k) {
		int result = 0;
		Map<Integer, Integer> map = new HashMap<>();
		Map<Integer, Integer> pairs = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {
			map.put(arr[i], i);
		}
		for (int i = 0; i < arr.length; i++) {
			int complement = k - arr[i];
			if (map.containsKey(complement) && map.get(complement) != i) {
				System.out.println(pairs.get(i) + " + " + pairs.get(complement));
				if (pairs.get(i) == null || pairs.get(complement) == null) {
					pairs.put(map.get(complement), i);
					pairs.put(i, map.get(complement));
					result++;
				}
			}
		}
		System.out.println("Result " + result);
		return result;
	}
}