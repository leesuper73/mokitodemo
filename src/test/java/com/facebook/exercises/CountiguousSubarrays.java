package com.facebook.exercises;

import java.util.Arrays;

public class CountiguousSubarrays {
	public static void main(String[] args) {

		int[] test_1 = { 3, 4, 1, 6, 2 };
		int[] test_2 = { 2, 4, 7, 1, 5, 3 };

		countSubarrays(test_1);
	}

	private static int[] countSubarrays(int[] arr) {
		int[] result = new int[arr.length];

		// bestStart and bestEnd to first index i.e 0

		int bestSum = arr[0];
		int bestStart = 0;
		int bestEnd = 0;

		// Initializing currentSum and currentStart to 0

		int currentSum = 0;
		int currentStart = 0;

		for (int i = 0; i < arr.length; i++) {
			// Adding current element to currentSum

			currentSum = currentSum + arr[i];

			// If currentSum becomes negative, clearing currentSum and
			// setting currentStart to next element

			if (currentSum < 0) {
				currentSum = 0;
				currentStart = i + 1;
			}

			// If currentSum exceeds bestSum, assigning currentSum to bestSum and
			// updating bestStart and bestEnd

			else if (currentSum > bestSum) {
				bestSum = currentSum;
				bestStart = currentStart;
				bestEnd = i;
			}
		}

		// Printing sub array with bestSum

		System.out.println("Input Array : " + Arrays.toString(arr));

		System.out.print("Continous Sub Array With Maximum Sum : ");

		System.out.print("[ ");

		for (int i = bestStart; i <= bestEnd; i++) {
			System.out.print(arr[i] + " ");
		}

		System.out.print("]");

		System.out.println();

		System.out.println("Sum : " + bestSum);
		return result;
	}
}