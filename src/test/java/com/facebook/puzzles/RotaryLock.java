package com.facebook.puzzles;

public class RotaryLock {

	public static void main(String args[]) {

		int N = 10;
		int M = 4;
		int[] C = { 9, 4, 4, 8 };

		getMinCodeEntryTime(N, M, C);
	}

	public static long getMinCodeEntryTime(int N, int M, int[] C) {
		int count = 0;

		int prev = 1;
		for (int i = 0; i < C.length; i++) {
			int dist1 = Math.abs(prev - C[i]);
			int dist2 = Math.abs(dist1 - N);
			int dist3 = (N - prev + C[i]) % N;

			System.out.println("dist3: " + dist3);
			// System.out.println("dist2: " + dist2);

			if (dist1 < dist2) {
				count += dist1;
			} else {
				count += dist2;
			}
			prev = C[i];
		}

		System.out.println("count: " + count);
		return count;
	}
}
