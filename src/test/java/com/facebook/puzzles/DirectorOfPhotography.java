package com.facebook.puzzles;

import java.util.HashMap;
import java.util.Map;

public class DirectorOfPhotography {

	public static void main(String args[]) {

		int N = 5;
		String C = "APABA";
		int X = 1;
		int Y = 2;

		getArtisticPhotographCount(N, C, X, Y);
	}

	public static int getArtisticPhotographCount(int N, String C, int X, int Y) {
		System.out.println("Photographs: " + N);
		System.out.println("Types: " + C);

		HashMap<String, String> photoMap = new HashMap();
		for (int i = 1; i <= N; i++) {
			photoMap.put(String.valueOf(i), String.valueOf(C.charAt(i - 1)));
			// System.out.println(i);
		}

		for (Map.Entry<String, String> set : photoMap.entrySet()) {
			System.out.println(set.getKey() + " = " + set.getValue());
		}
		return 0;
	}
}
