package com.facebook.puzzles;

public class StackStabilization {

	public static void main(String args[]) {

		int N = 4;
		int[] R = { 6, 5, 4, 3 };

		getMinimumDeflatedDiscCount(N, R);
	}

	public static int getMinimumDeflatedDiscCount(int N, int[] R) {
		int count = 0;
		int prev = 0;
		for (int i = R.length - 1; i >= 0; i--) {
			if (prev == 0) {
				R[i] = R[i];
			} else {
				if (prev <= R[i]) {
					count++;
					R[i] = R[i] - (R[i] - prev + 1);
					if (R[i] < 1) {
						count = -1;
						break;
					}
				}
			}
			prev = R[i];
		}
		return count;
	}

}
