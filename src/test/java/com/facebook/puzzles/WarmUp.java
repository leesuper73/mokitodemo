package com.facebook.puzzles;

import java.util.ArrayList;
import java.util.Arrays;

public class WarmUp {

	public static void main(String args[]) {

		// System.out.println(getWrongAnswers(3, "AAA"));

		/*
		 * int R = 0; int C = 2; int[][] g = { { 0, 0, 1 }, { 1, 0, 1 } };
		 */

		// System.out.println(getHitProbability(0, 1, g));

		long totalSeats = 10;
		long distance = 1;
		int diners = 2;
		long[] seats = { 2, 6 };
		System.out.println(getMaxAdditionalDinersCount(totalSeats, distance, diners, seats));

	}

	private static int count = 0;

	public static long getMaxAdditionalDinersCount(long N, long K, int M, long[] S) {
		int count = 0;

		// Populate seats
		ArrayList<Long> seats = new ArrayList<Long>();
		for (long i = 1; i <= N; i++) {
			seats.add(i);
		}

		// Remove seats that are not available
		for (Long seat : S) {
			seats.remove(seat);
		}

		long total = K * 2;
		System.out.println("total " + total);
		getSeats(seats, K);

		System.out.println("Count " + count);
		System.out.println("Available Seat # " + Arrays.toString(seats.toArray()));

		return count;
	}

	public static ArrayList<Long> getSeats(ArrayList<Long> seats, long K) {
		long total = K * 2;
		for (long seat : seats) {
			long frontCnt = 0;
			long backCnt = 0;
			System.out.println("seat " + seat);
			for (long i = 1; i <= K; ++i) {

				System.out.println("F " + (seat - i));
				System.out.println("B " + (seat + i));

				if (seats.contains(seat - i)) {
					frontCnt++;
				}
				if (seats.contains(seat + i)) {
					backCnt++;
				}
				if (frontCnt + backCnt == total) {
					count++;
					frontCnt = 0;
					backCnt = 0;
					System.out.println("Count++");
				}
				System.out.println("add " + (frontCnt + backCnt));
				System.out.println("backCnt " + backCnt);
				System.out.println("frontCnt " + frontCnt);
			}
		}
		return seats;
	}

	public static double getHitProbability(int R, int C, int[][] G) {
		float cellCount = 0;
		float shipCount = 0;

		for (int row = 0; row < G.length; row++) {
			for (int col = 0; col < G[row].length; col++) {

				System.out.println("G[row][col]: " + row + " " + col + " " + G[row][col]);

				if (G[row][col] == 1) {
					shipCount++;
				}
				cellCount++;
			}
		}

		System.out.println("Cell Count: " + cellCount);
		System.out.println("Ship Count: " + shipCount);

		System.out.println("Percent: " + (shipCount / cellCount));

		// Write your code here
		return 0.0;
	}

	public static String getWrongAnswers(int N, String C) {
		// Write your code here
		StringBuffer result = new StringBuffer();
		for (int i = 1; i < N + 1; i++) {
			char charAt = C.charAt(i - 1);
			String answer = String.valueOf(charAt);

			if (answer.equalsIgnoreCase("B")) {
				result.append("A");
			} else {
				result.append("B");
			}
		}
		return result.toString();
	}

}
