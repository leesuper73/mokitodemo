package com.facebook.puzzles;

import java.util.ArrayList;

public class Kaitenzushi {

	public static void main(String args[]) {

		int N = 6;
		int[] D = { 1, 2, 3, 3, 2, 1 };
		int K = 2;

		getMaximumEatenDishCount(N, D, K);
	}

	public static int getMaximumEatenDishCount(int N, int[] D, int K) {

		int count = 0;
		int lastEaten = 0;
		for (int i = 0; i < D.length; i++) {
			ArrayList<Integer> prev = new ArrayList<Integer>();
			prev.add(lastEaten);
			for (int j = 1; j <= K; j++) {
				if ((i - j) >= 0) {
					lastEaten = D[i - j];
					prev.add(D[i - j]);
				}
			}
			if (!prev.contains(D[i])) {
				count++;
			}
		}
		System.out.println(count);
		return count;
	}
}
