package com.facebook.puzzles;

public class ScoreBoardInterface1 {

	public static void main(String args[]) {

		int N = 6;
		int[] S = { 1, 2, 3, 4, 5, 6 };

		getMinProblemCount(N, S);
	}

	public static int getMinProblemCount(int N, int[] S) {
		int count = 0;

		for (int i = 0; i < S.length; i++) {
			System.out.println("S: " + S[i]);
		}

		System.out.println("count: " + count);
		return count;
	}
}
