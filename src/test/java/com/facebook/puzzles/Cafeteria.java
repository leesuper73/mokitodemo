package com.facebook.puzzles;

public class Cafeteria {

	public static void main(String args[]) {

		long totalSeats = 10;
		long distance = 1;
		int diners = 2;
		long[] seats = { 2, 6 };
		System.out.println(getMaxAdditionalDinersCount(totalSeats, distance, diners, seats));

	}

	public static long getMaxAdditionalDinersCount(long N, long K, int M, long[] S) {
		int count = 0;

		long[] seats = new long[(int) N];
		for (Long seat : S) {
			seats[(int) (seat - 1)] = 1;
		}

		int total = (int) (K * 2); // Total seat space need
		for (int i = 0; i < seats.length; i++) {
			if (seats[i] < 1) { // If unoccupied seat
				int beforeCnt = 0; // Seats available before
				int afterCnt = 0; // Seats available after
				for (int j = 1; j <= K; ++j) { // Count seats available
					if ((i - j) > -1) { // Seats before
						if (seats[i - j] == 0) { // Seat is empty
							beforeCnt++;
						} else {
							j = (int) K;
							break; // Skip to next
						}
					} else {
						beforeCnt++;
					}

					if ((i + j) < seats.length - 1) { // Seats After
						if (seats[i + j] == 0) { // Seat is empty
							afterCnt++;
						} else {
							j = (int) K;
							break; // Skip to next
						}
					} else {
						afterCnt++;
					}
					if (beforeCnt + afterCnt == total) {
						seats[i] = 1; // Take seat
						count++;
						beforeCnt = 0;
						afterCnt = 0;
					}
				}
			}
		}
		return count;
	}

}
