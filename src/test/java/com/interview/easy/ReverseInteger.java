package com.interview.easy;

public class ReverseInteger {
	public static void main(String[] args) {

		int x = 1234;
		System.out.println(reverse(x));
	}

	public static int reverse(int x) {
		long out = 0; // result might overflow
		while (x != 0) {
			System.out.println(out * 10);
			out = out * 10 + x % 10; // append last digit of x
			x = x / 10; // remove last digit
		}
		if (out > Integer.MAX_VALUE || out < Integer.MIN_VALUE)
			return 0;
		return (int) out;
	}
}
